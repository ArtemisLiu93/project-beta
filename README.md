# CarCar

Team:

- Person 1 - Which microservice?
  Artemis Liu - Sales

- Person 2 - Which microservice?
  Christopher Bustamante - Services

## Design

## Service microservice

Explain your models and integration with the inventory
microservice, here.

I created my models Appointment, Technician, and AutomobileVO. AutomobileVO is where i used a poll file to poll my data from inventory and for each car created a VO with my needs. I then used insomnia to see my endpoints and what my outcome was going to be when receiving my json. On the front end i fetched all my data and displayed it dynamically, i also created a search bar that is fully functional with a look up while typing. You do have to type in capital letters just as the VIN is written.

## Sales microservice

Explain your models and integration with the inventory
microservice, here.
Salesperson: This model represents a salesperson and has fields such as first_name, last_name, and employee_id. The employee_id field is unique and serves as the primary key for each salesperson.
Customer: This model represents a customer and has fields such as first_name, last_name, address, and phone_number. The phone_number field is unique and serves as the primary key for each customer.
Sale: This model represents a sale transaction and includes fields such as automobile, salesperson, customer, and price. The automobile field is a foreign key to the AutomobileVO model, representing the automobile being sold. The salesperson and customer fields are foreign keys to the Salesperson and Customer models, respectively. The price field represents the price of the sale.
AutomobileVO: This model represents an automobile and includes fields such as vin (Vehicle Identification Number) and sold. The vin field is unique and serves as the primary key for each automobile. The sold field is a boolean indicating whether the automobile has been sold or not.
Created in views with all the endpoint HTTP requests for me to fetch the API request and upload the data in the browser
