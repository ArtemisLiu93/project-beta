from .models import Technician, Appointment
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .encoders import AppointmentEncoder, TechnicianEncoder
import json


@require_http_methods(["GET", "POST"])
def show_technicians(request):
    if request.method == "GET":
        try:
            technicians = Technician.objects.all()
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Technician does not exist"},
                status=400,
            )

        return JsonResponse({"technicians": technicians}, encoder=TechnicianEncoder, safe=False)
    else:
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(technician, encoder=TechnicianEncoder, safe=False)



@require_http_methods(["GET", "DELETE"])
def show_specific_technicician(request, id):
    if request.method == "GET":
        try:
            technician = Technician.objects.get(id=id)
            return JsonResponse(technician, encoder=TechnicianEncoder, safe=False)
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Technician does not exist"},
                status=400,
            )
    else:
        try:
            count, _ = Technician.objects.get(id=id).delete()
            return JsonResponse({"deleted:": count > 0})
        except Technician.DoesNotExist:
            return JsonResponse({"message": "Technician does not exist"}, status=400)


@require_http_methods(["GET", "POST"])
def get_appointments(request):
    if request.method == "GET":
        try:
            appointments = Appointment.objects.all()
        except Appointment.DoesNotExist:
            return JsonResponse({"message": "Appointment doesn't exist"}, status=400)
        return JsonResponse({"appointments": appointments}, encoder=AppointmentEncoder, safe=False)
    else:
        content = json.loads(request.body)
        try:
            technician = Technician.objects.get(id=content["technician"])
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse({"message": "Technician does not exist!"}, status=400)
        appointment = Appointment.objects.create(**content)
        return JsonResponse(appointment, encoder=AppointmentEncoder, safe=False)


@require_http_methods(["GET", "DELETE"])
def get_specific_appointment(request, id):
    if request.method == "GET":
        try:
            appointment = Appointment.objects.get(id=id)
            return JsonResponse(appointment, encoder=AppointmentEncoder, safe=False)
        except Appointment.DoesNotExist:
            return JsonResponse({"message": "Appointment does not exist!"}, status=400)
    else:
        try:
            count, _ = Appointment.objects.get(id=id).delete()
            return JsonResponse({"deleted:": count > 0})
        except Appointment.DoesNotExist:
            return JsonResponse({"message": "Appointment does not exist"}, status=400)


@require_http_methods(["PUT"])
def cancel_appointment(request, id):
    if request.method == "PUT":
        try:
            appointment = Appointment.objects.get(id=id)
            appointment.status = "canceled"
            appointment.save()
        except Appointment.DoesNotExist:
            return JsonResponse({"message": "Appointment does not exist!"}, status=400)
        return JsonResponse(appointment, encoder=AppointmentEncoder, safe=False)


@require_http_methods(["PUT"])
def finish_appointment(request, id):
    if request.method == "PUT":
        try:
            appointment = Appointment.objects.get(id=id)
            appointment.status = "finished"
            appointment.save()
        except Appointment.DoesNotExist:
            return JsonResponse({"message": "Appointment does not exist!"}, status=400)
        return JsonResponse(appointment, encoder=AppointmentEncoder, safe=False)
