from django.db import models

completion_choices = (('Created','Created'), ('Finished','Finished'), ('Canceled','Canceled'))


class Technician(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.CharField(max_length=100, unique=True)


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)


class Appointment(models.Model):
    date_time = models.DateTimeField(auto_now_add=False)
    reason = models.CharField(max_length=200)
    status = models.CharField(max_length=50, choices=completion_choices, default="Created")
    vin = models.CharField(max_length=17)
    customer = models.CharField(max_length=100)
    technician = models.ForeignKey(Technician, on_delete=models.CASCADE)