from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import AutomobileVO, Salesperson, Customer, Sale
from .encoders import AutomobileDetailEncoder, SalespersonDetailEncoder, CustomerDetailEncoder, SaleDetailEncoder
import json


@require_http_methods(["GET"])
def automobile_list(request):
    if request.method == "GET":
        automobiles = AutomobileVO.objects.all()
        return JsonResponse(
            {"automobiles": automobiles},
            encoder=AutomobileDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def sales_list(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SaleDetailEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        try:
            customer_id = content["customer"]
            customer = Customer.objects.get(id=customer_id)
            content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer does not exist"},
                status=404,
            )
        try:
            salesperson = content["salesperson"]
            singular_sale = Salesperson.objects.get(employee_id=salesperson)
            content["salesperson"] = singular_sale
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Sales Person does not exist"},
                status=404,
            )
        try:
            automobile_vin = content["automobile"]
            auto = AutomobileVO.objects.get(vin=automobile_vin)
            content['automobile'] = auto
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Automobile does not exist"},
                status=404,
            )
        sale = Sale.objects.create(**content)
        return JsonResponse(
            sale,
            encoder=SaleDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE"])
def api_sales(request, id):
    if request.method == "GET":
        try:
            sale = Sale.objects.get(id=id)
            return JsonResponse(
                sale,
                encoder=SaleDetailEncoder,
                safe=False,
            )
        except Sale.DoesNotExist:
            return JsonResponse(
                {"message": "This sale does not exist"},
                status=404,
            )
    elif request.method == "DELETE":
        try:
            sale = Sale.objects.get(id=id).delete()
            return JsonResponse(
                {"message": "Deleted"},
                status=204,
            )
        except Sale.DoesNotExist:
            return JsonResponse(
                {"message": "This sale does not exist"},
                status=404,
            )


@require_http_methods(["GET", "POST"])
def customer_list(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerDetailEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE"])
def api_customer(request, id):
    count, _ = Customer.objects.filter(id=id).delete()
    return JsonResponse(
        {"deleted": count > 0},
        status=204,
        )


@require_http_methods(["GET", "POST"])
def api_salespeople(request):
    if request.method == "GET":
        salespeople = Salesperson.objects.all()
        return JsonResponse(
            {"salespeople": salespeople},
            encoder=SalespersonDetailEncoder,
        )
    elif request.method == "POST":
        content = json.loads(request.body)
        salesperson = Salesperson.objects.create(**content)
        return JsonResponse(
            {"salesperson": salesperson},
            encoder=SalespersonDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE"])
def api_salesperson(request, id):
    count, _ = Salesperson.objects.filter(id=id).delete()
    return JsonResponse(
        {"deleted": count > 0},
        status=204,
    )
