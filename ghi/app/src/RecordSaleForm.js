import React, { useEffect, useState } from 'react';

function RecordSaleForm() {
const [salesperson, setSalesperson] = useState('');
const [salespeople, setSalespeople] = useState([]);
const [automobile, setAutomobile] = useState('');
const [automobiles, setAutomobiles] = useState([]);
const [customer, setCustomer] = useState('');
const [customers, setCustomers] = useState([]);
const [price, setPrice] = useState('');

const automobileChange = (event) => {
    setAutomobile(event.target.value);
};

const personChange = (event) => {
    setSalesperson(event.target.value);
};

const customerChange = (event) => {
    setCustomer(event.target.value);
};

const priceChange = (event) => {
    setPrice(event.target.value);
};

const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.price = price;
    data.salesperson = salesperson;
    data.customer = customer;
    data.automobile = automobile;


    const salesUrl = 'http://localhost:8090/api/sales/';
    const fetchConfig = {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        },
    };

    const response = await fetch(salesUrl, fetchConfig);
    if (response.ok) {
        const newSale = await response.json()
        setPrice('');
        setAutomobile('');
        setSalesperson('');
        setCustomer('');
        }
    };

async function fetchAutomobiles() {
    const automobileUrl = 'http://localhost:8100/api/automobiles/';
    const response = await fetch(automobileUrl);
    if (response.ok) {
    const data = await response.json();
    setAutomobiles(data.autos);
    }
}

async function fetchSalespeople() {
    const salespersonUrl = 'http://localhost:8090/api/salespeople/';
    const response = await fetch(salespersonUrl);
    if (response.ok) {
    const data = await response.json();
    setSalespeople(data.salespeople);
    }
}

async function fetchCustomers() {
    const customerUrl = 'http://localhost:8090/api/customers/';
    const response = await fetch(customerUrl);
    if (response.ok) {
    const data = await response.json();
    setCustomers(data.customers);
    }
}

useEffect(() => {
    fetchAutomobiles();
    fetchSalespeople();
    fetchCustomers();
}, []);


return (
<div className="row">
<div className="offset-3 col-6">
    <div className="shadow p-4 mt-4">
    <h1>Record a New Sale</h1>
    <form id="create-record-of-sale-form" onSubmit={handleSubmit}>
        <label htmlFor="automobile_vin">Automobile VIN</label>
    </form>
    </div>

    <div className="row">
    <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
        <form id="create-shoes-form" onSubmit={handleSubmit}>
        <div className="form-floating mb-3">
            <select
                value={automobile}
                placeholder="Automobile"
                required
                type="text"
                name="automobile"
                id="automobile"
                className="form-control"
                onChange={automobileChange}
            >
                <option value="">Choose an automobile VIN</option>
                {automobiles?.map((vehicle, index) => {
                return (
                    <option key={index} value={vehicle.vin}>
                    {vehicle.vin}
                    </option>
                );
                })}
            </select>
            </div>

            <div className="form-floating mb-3">
            <select
                value={salesperson}
                placeholder="Salesperson"
                required
                name="salesperson"
                id="salesperson"
                className="form-control"
                onChange={personChange}
            >
                <option value="">Choose a Salesperson</option>
                {salespeople?.map((sales) => {
                return (
                    <option key={sales.id} value={sales.id}>
                    {sales.first_name + " " + sales.last_name }
                    </option>
                );
                })}
            </select>
            </div>

            <div className="form-floating mb-3">
            <select
                value={customer}
                placeholder="Customer"
                required
                name="customer"
                id="customer"
                className="form-control"
                onChange={customerChange}
            >
                <option value="">Choose a customer</option>
                {customers?.map((people) => {
                return (
                    <option key={people.id} value={people.id}>
                    {people.first_name + " " + people.last_name }
                    </option>
                );
                })}
            </select>
            </div>

            <div className="form-floating mb-3">
            <input
                value={price}
                placeholder="Price"
                required
                type="text"
                id="price"
                name="price"
                className="form-control"
                onChange={priceChange}
            />
            <label htmlFor="price">Price</label>
            </div>
            <button className="btn btn-primary">Create</button>
        </form>
        </div>
    </div>
    </div>
</div>
</div>
);
}


export default RecordSaleForm;