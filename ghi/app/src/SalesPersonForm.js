import React, { useEffect, useState } from 'react';

function SalesForm() {
    const [first_name, setFirstName] = useState('');
    const [last_name, setLastName] = useState('');
    const [employee_id, setEmployeeId] = useState('');

    const firstNameChange = (event) => {
        const value = event.target.value;
        setFirstName(value);
    }
    const lastNameChange = (event) => {
        const value = event.target.value;
        setLastName(value);
    }
    const employeeIdChange = (event) => {
        const value = event.target.value;
        setEmployeeId(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.first_name = first_name;
        data.last_name = last_name;
        data.employee_id = employee_id;
        const salespersonUrl = 'http://localhost:8090/api/salespeople/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
        }
    };
    const response = await fetch(salespersonUrl, fetchConfig);
    if (response.ok) {
        const newSalesperson = await response.json();
        setFirstName('');
        setLastName('');
        setEmployeeId('');
        }
    };
    
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add A Salesperson</h1>
                    <form id="create-shoes-form" onSubmit={handleSubmit}>
                        <div className="form-floating mb-3">
                            <input
                                value={first_name}
                                placeholder="First Name"
                                required
                                type="text"
                                name="first_name"
                                id="first_name"
                                className="form-control"
                                onChange={firstNameChange}
                            />
                            <label htmlFor="first_name">First Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                value={last_name}
                                placeholder="Last Name"
                                required
                                type="text"
                                name="last_name"
                                id="last_name"
                                className="form-control"
                                onChange={lastNameChange}
                            />
                            <label htmlFor="last_name">Last Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                value={employee_id}
                                placeholder="Employee ID"
                                required
                                type="text"
                                id="employee_id"
                                name="employee_id"
                                className="form-control"
                                onChange={employeeIdChange}
                            />
                            <label htmlFor="employee_id">Employee ID</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default SalesForm;