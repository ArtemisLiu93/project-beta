import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import CustomerForm from "./CustomerForm";
import RecordSaleForm from "./RecordSaleForm";
import SalesPersonForm from "./SalesPersonForm";
import CustomerList from "./CustomerList";
import SalesList from "./SalesList";
import SalesPersonList from "./SalesPersonList";
import SalespersonHistory from "./SalespersonHistory";
import TechnicianForm from "./ServiceSide/technicianForm";
import TechniciansList from "./ServiceSide/technicianList";
import AppointmentForm from "./ServiceSide/appointmentForm";
import ServicesList from "./ServiceSide/serviceHistory";
import ServicesAppointments from "./ServiceSide/serviceAppointments";
import ManufacturerForm from "./Inventory/manufacturerForm";
import ModelsList from "./Inventory/modelsList";
import AutomobilesList from "./Inventory/automobilesList";
import ManufacturersList from "./Inventory/manufacturersList";
import ModelForm from "./Inventory/modelForm";
import AutomobileForm from "./Inventory/automobilesForm";

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="customers/create" element={<CustomerForm />} />
          <Route path="sales/create" element={<RecordSaleForm />} />
          <Route path="salespeople/create" element={<SalesPersonForm />} />
          <Route path="customers/" element={<CustomerList />} />
          <Route path="sales/" element={<SalesList />} />
          <Route path="salespeople/" element={<SalesPersonList />} />
          <Route path="sales/history/" element={<SalespersonHistory />} />
          <Route path="" element={<MainPage />} />
          <Route path="manufacturers/create" element={<ManufacturerForm />} />
          <Route path="models" element={<ModelsList />} />
          <Route path="automobiles" element={<AutomobilesList />} />
          <Route path="automobiles/create" element={<AutomobileForm />} />
          <Route path="manufacturers" element={<ManufacturersList />} />
          <Route path="models/create" element={<ModelForm />} />
          <Route path="technicians/">
            <Route path="" element={<TechniciansList />} />
            <Route path="create" element={<TechnicianForm />} />
          </Route>
          <Route path="appointments/">
            <Route path="" element={<ServicesAppointments />} />
            <Route path="history" element={<ServicesList />} />
            <Route path="create" element={<AppointmentForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
