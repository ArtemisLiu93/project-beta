import React from "react";
import { NavLink } from "react-router-dom";

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">
          CarCar
        </NavLink>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link" aria-current="page" to="/">
                Home
              </NavLink>
            </li>
            <div className="dropdown nav-item">
              <button
                className="btn dropdown-toggle nav-item"
                type="button"
                id="dropdownMenuButton1"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Sales
              </button>
              <ul
                className="dropdown-menu"
                aria-labelledby="dropdownMenuButton1"
              >
                <li className="dropdown-item nav-item">
                  <NavLink className="nav-link" to="/customers/create/">
                    Add a Customer
                  </NavLink>
                </li>
                <li className="dropdown-item nav-item">
                  <NavLink className="nav-link" to="/salespeople/create">
                    Add a Salesperson
                  </NavLink>
                </li>
                <li className="dropdown-item nav-item">
                  <NavLink className="nav-link" to="/sales/create">
                    Record a Sale
                  </NavLink>
                </li>
                <li className="dropdown-item nav-item">
                  <NavLink className="nav-link" to="/salespeople">
                    List of Salespeople
                  </NavLink>
                </li>
                <li className="dropdown-item nav-item">
                  <NavLink className="nav-link" to="/customers">
                    List of Customers
                  </NavLink>
                </li>
                <li className="dropdown-item nav-item">
                  <NavLink className="nav-link" to="/sales">
                    List of Sales
                  </NavLink>
                </li>
                <li className="dropdown-item nav-item">
                  <NavLink className="nav-link" to="/sales/history">
                    List of Salesperson History
                  </NavLink>
                </li>
              </ul>
            </div>
            <div className="dropdown nav-item">
              <button
                className="btn dropdown-toggle nav-item"
                type="button"
                id="dropdownMenuButton1"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Services
              </button>
              <ul
                className="dropdown-menu"
                aria-labelledby="dropdownMenuButton1"
              >
                <li className="dropdown-item nav-item">
                  <NavLink className="nav-link" to="/technicians">
                    Technician list
                  </NavLink>
                </li>
                <li className="dropdown-item nav-item">
                  <NavLink className="nav-link" to="/technicians/create">
                    Create a technician
                  </NavLink>
                </li>
                <li className="dropdown-item nav-item">
                  <NavLink className="nav-link" to="/appointments/create">
                    Create an Appointment
                  </NavLink>
                </li>
                <li className="dropdown-item nav-item">
                  <NavLink className="nav-link" to="/appointments/history">
                    View Service History
                  </NavLink>
                </li>
                <li className="dropdown-item nav-item">
                  <NavLink className="nav-link" to="/appointments/">
                    Service Appointments
                  </NavLink>
                </li>
              </ul>
            </div>
            <div className="dropdown nav-item">
              <button
                className="btn dropdown-toggle nav-item"
                type="button"
                id="dropdownMenuButton1"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Inventory
              </button>
              <ul
                className="dropdown-menu"
                aria-labelledby="dropdownMenuButton1"
              >
                <li className="dropdown-item nav-item">
                  <NavLink className="nav-link" to="manufacturers/create">
                    Create Manufacturer
                  </NavLink>
                </li>
                <li className="dropdown-item nav-item">
                  <NavLink className="nav-link" to="manufacturers">
                    Manufacturers List
                  </NavLink>
                </li>
                <li className="dropdown-item nav-item">
                  <NavLink className="nav-link" to="models/create">
                    Create A Vehicle Model
                  </NavLink>
                </li>
                <li className="dropdown-item nav-item">
                  <NavLink className="nav-link" to="models">
                    Models List
                  </NavLink>
                </li>
                <li className="dropdown-item nav-item">
                  <NavLink className="nav-link" to="automobiles">
                    Automobile List
                  </NavLink>
                </li>
                <li className="dropdown-item nav-item">
                  <NavLink className="nav-link" to="automobiles/create">
                    Automobile Form
                  </NavLink>
                </li>
              </ul>
            </div>
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
