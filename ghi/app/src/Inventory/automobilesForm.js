import React, { useState, useEffect } from "react";
export default function AutomobileForm() {
  const [color, setColor] = useState("");
  const colorChanged = (event) => {
    setColor(event.target.value);
  };
  const [year, setYear] = useState("");
  const yearChange = (event) => {
    setYear(event.target.value);
  };
  const [vin, setVIN] = useState("");
  const vinChange = (event) => {
    setVIN(event.target.value);
  };
  const [model_id, setModel] = useState("");
  const modelChange = (event) => {
    setModel(event.target.value);
  };
  const [models, setModels] = useState([]);
  const getModels = async () => {
    const response = await fetch("http://localhost:8100/api/models/");
    if (response.ok) {
      const data = await response.json();
      setModels(data.models);
    }
  };
  useEffect(() => {
    getModels();
  }, []);
  const submitForm = async (event) => {
    event.preventDefault();
    const body = {
      color,
      year,
      vin,
      model_id,
    };
    const AutoUrl = "http://localhost:8100/api/automobiles/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(body),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(AutoUrl, fetchConfig);
    if (response.ok) {
      const newAuto = await response.json();
      console.log(newAuto);
      setColor("");
      setYear("");
      setVIN("");
      setModel("");
    }
  };
  return (
    <div className="row">
      <div className="offset-3 col-4 m-auto">
        <div className="shadow-lg rounded p-4 mt-5">
          <h2 className="text-center mb-2 text-muted">
            Add an Automobile to Inventory
          </h2>
          <form id="create-location-form" onSubmit={submitForm}>
            <div className="form-floating mb-3">
              <input
                placeholder="Color"
                required
                type="text"
                id="color"
                className="form-control"
                name="color"
                value={color}
                onChange={colorChanged}
              />
              <label htmlFor="Color">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input
                placeholder="Year"
                required
                type="text"
                id="year"
                className="form-control"
                name="year"
                value={year}
                onChange={yearChange}
              />
              <label htmlFor="Year">Year</label>
            </div>
            <div className="form-floating mb-3">
              <input
                placeholder="Vin"
                required
                type="text"
                id="vin"
                className="form-control"
                name="vin"
                value={vin}
                onChange={vinChange}
              />
              <label htmlFor="Vin">Vin</label>
            </div>
            <div className="mb-3">
              <select
                required
                id="model_id"
                className="form-select"
                name="model_id"
                value={model_id}
                onChange={modelChange}
              >
                <option>Choose a model</option>
                {models.map((model) => {
                  return (
                    <option key={model.id} value={model.id}>
                      {model.name}
                    </option>
                  );
                })}
              </select>
            </div>
            <div className="text-center">
              <button className="btn btn-secondary">Create</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}
