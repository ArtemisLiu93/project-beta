import React, { useEffect, useState } from "react";

export default function ManufacturerForm() {
  const [name, setName] = useState("");
  const changeName = (event) => {
    setName(event.target.value);
  };
  const submitForm = async (event) => {
    event.preventDefault();
    const body = {
      name,
    };
    const manufacturerUrl = "http://localhost:8100/api/manufacturers/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(body),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(manufacturerUrl, fetchConfig);
    if (response.ok) {
      const newManufacturer = await response.json();
      console.log(newManufacturer);
      setName("");
    }
  };
  return (
    <div className="row">
      <div className="offset-3 col-4 m-auto">
        <div className="shadow-lg rounded p-4 mt-5">
          <h2 className="text-center mb-2 text-muted">
            Create a new Manufacturer
          </h2>
          <form id="create-location-form" onSubmit={submitForm}>
            <div className="form-floating mb-3">
              <input
                placeholder="Name"
                required
                type="text"
                id="name"
                className="form-control"
                name="name"
                value={name}
                onChange={changeName}
              />
              <label htmlFor="name">Name</label>
            </div>
            <div className="text-center">
              <button className="btn btn-secondary">Create</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}
