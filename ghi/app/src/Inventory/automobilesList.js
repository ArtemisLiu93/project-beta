import { useEffect, useState, Fragment } from "react";
export default function AutomobilesList() {
  const [automobiles, setAutos] = useState([]);
  const getAutos = async () => {
    const autosResponse = await fetch("http://localhost:8100/api/automobiles/");
    if (autosResponse.ok) {
      const autos = await autosResponse.json();
      setAutos(autos.autos);
    }
  };
  useEffect(() => {
    getAutos();
  }, []);

  return (
    <Fragment>
      <div className="m-4">
        <div>
          <h1>Automobiles</h1>
        </div>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>VIN</th>
              <th>Color</th>
              <th>Year</th>
              <th>Model</th>
              <th>Manufacturer</th>
              <th>Sold</th>
            </tr>
          </thead>
          <tbody>
            {automobiles.map((auto) => {
              const sold = auto.sold === true ? "Yes" : "No";
              return (
                <tr key={auto.id}>
                  <td>{auto.vin}</td>
                  <td>{auto.color}</td>
                  <td>{auto.year}</td>
                  <td>{auto.model.name}</td>
                  <td>{auto.model.manufacturer.name}</td>
                  <td>{sold}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </Fragment>
  );
}
