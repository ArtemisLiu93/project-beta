import { useEffect, useState, Fragment } from "react";
export default function ManufacturersList() {
  const [manufacturers, setManus] = useState([]);
  const getManus = async () => {
    const autosResponse = await fetch(
      "http://localhost:8100/api/manufacturers/"
    );
    if (autosResponse.ok) {
      const manus = await autosResponse.json();
      setManus(manus.manufacturers);
    }
  };
  useEffect(() => {
    getManus();
  }, []);

  return (
    <Fragment>
      <div className="m-4">
        <div>
          <h1>Manufacturers</h1>
        </div>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Name</th>
            </tr>
          </thead>
          <tbody>
            {manufacturers.map((manu) => {
              return (
                <tr key={manu.id}>
                  <td>{manu.name}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </Fragment>
  );
}
