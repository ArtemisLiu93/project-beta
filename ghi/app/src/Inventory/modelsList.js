import { useEffect, useState, Fragment } from "react";
import "../index.css";
export default function ModelsList() {
  const [models, setModels] = useState([]);
  const getModels = async () => {
    const modelsResponse = await fetch("http://localhost:8100/api/models/");
    if (modelsResponse.ok) {
      const models = await modelsResponse.json();
      setModels(models.models);
    }
  };
  useEffect(() => {
    getModels();
  }, []);

  return (
    <Fragment>
      <div className="m-4">
        <div>
          <h1>Models</h1>
        </div>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Name</th>
              <th>Manufacturer</th>
              <th>Picture</th>
            </tr>
          </thead>
          <tbody>
            {models.map((model) => {
              return (
                <tr key={model.id} className="align-center">
                  <td className="w-33 mh-100">{model.name}</td>
                  <td className="w-33 mh-100">{model.manufacturer.name}</td>
                  <td className="w-33 mh-100">
                    <img src={model.picture_url} alt="" />
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </Fragment>
  );
}
