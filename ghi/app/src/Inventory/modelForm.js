import React, { useState, useEffect } from "react";

export default function ModelForm() {
  const [name, setName] = useState("");
  const changeName = (event) => {
    setName(event.target.value);
  };
  const [picture_url, setPicture] = useState("");
  const changePicture = (event) => {
    setPicture(event.target.value);
  };
  const [manufacturer_id, setManu] = useState("");
  const changeManu = (event) => {
    setManu(event.target.value);
  };
  const [manufacturers, setManus] = useState([]);
  const getManufacturers = async () => {
    const manusResponse = await fetch(
      "http://localhost:8100/api/manufacturers/"
    );
    if (manusResponse.ok) {
      const manufacturers = await manusResponse.json();
      console.log(manufacturers.manufacturers);
      setManus(manufacturers.manufacturers);
    }
  };
  useEffect(() => {
    getManufacturers();
  }, []);
  const submitForm = async (event) => {
    event.preventDefault();
    const body = {
      name,
      picture_url,
      manufacturer_id,
    };
    const modelUrl = "http://localhost:8100/api/models/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(body),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(modelUrl, fetchConfig);
    if (response.ok) {
      const newModel = await response.json();
      console.log(newModel);
      setName("");
      setPicture("");
      setManu("");
    }
  };
  return (
    <div className="row">
      <div className="offset-3 col-4 m-auto">
        <div className="shadow-lg rounded p-4 mt-5">
          <h2 className="text-center mb-2 text-muted">
            Create a vehicle Model
          </h2>
          <form id="create-location-form" onSubmit={submitForm}>
            <div className="form-floating mb-3">
              <input
                placeholder="Name"
                required
                type="text"
                id="name"
                className="form-control"
                name="name"
                value={name}
                onChange={changeName}
              />
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                placeholder="Name"
                required
                type="text"
                id="picture_url"
                className="form-control"
                name="picture_url"
                value={picture_url}
                onChange={changePicture}
              />
              <label htmlFor="name">Name</label>
            </div>
            <div className="mb-3">
              <select
                required
                id="manufacturer_id"
                className="form-select"
                name="manufacturer_id"
                value={manufacturer_id}
                onChange={changeManu}
              >
                <option>Choose a manufacturer</option>
                {manufacturers.map((manus) => {
                  return (
                    <option key={manus.id} value={manus.id}>
                      {manus.name}
                    </option>
                  );
                })}
              </select>
            </div>
            <div className="text-center">
              <button className="btn btn-secondary" onClick={submitForm}>
                Create
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}
