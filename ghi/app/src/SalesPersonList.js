import React, { useState, useEffect } from "react";

function SalesPersonList() {
    const [salespeople, setSalespeople] = useState([]);

    useEffect(() => {
        fetchSalespeople();
    }, []);

    const fetchSalespeople = async () => {
        try {
            const response = await fetch('http://localhost:8090/api/salespeople/');
            if (response.ok) {
                const data = await response.json();
                setSalespeople(data.salespeople);
            } else {
                console.error('Failed to fetch Salespeople');
            }
        } catch (error) {
            console.error('Error:', error);
        }
    };

    return (
        <div>
            <h1>Salespeople</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Employee ID</th>
                    </tr>
                </thead>
                <tbody>
                    {salespeople.map((salespeople) => (
                        <tr key={salespeople.id}>
                            <td>{salespeople.first_name}</td>
                            <td>{salespeople.last_name}</td>
                            <td>{salespeople.employee_id}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
}

export default SalesPersonList;