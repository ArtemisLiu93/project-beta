import { useEffect, useState, Fragment } from "react";
export default function ServicesList() {
  const [services, setServices] = useState([]);
  const getAppointments = async () => {
    const appointmentsResponse = await fetch(
      "http://localhost:8080/api/appointments/"
    );
    if (appointmentsResponse.ok) {
      const data = await appointmentsResponse.json();
      setServices(data.appointments);
    }
  };
  const [filtered, setFiltered] = useState([]);
  const filteredServices = () => {
    let filteredServices = [];
    for (let service of services) {
      if (service.vin.includes(search)) {
        filteredServices.push(service);
        setFiltered(filteredServices);
      }
      if (search === "") {
        filteredServices = [...services];
        setFiltered(filteredServices);
      }
    }
  };
  useEffect(() => {
    filteredServices();
  }, [services]);
  const [search, setSearch] = useState("");
  const changeSearch = (event) => {
    console.log(event.target.value);
    setSearch(event.target.value);
  };
  useEffect(() => {
    getAppointments();
  }, [services]);

  const [autos, setAutos] = useState([]);
  const getAutomobiles = async () => {
    const autosResponse = await fetch("http://localhost:8100/api/automobiles/");
    if (autosResponse.ok) {
      const autoData = await autosResponse.json();
      let vins = [];
      for (let i = 0; i < autoData.autos.length; i++) {
        vins.push(autoData.autos[i].vin);
      }
      setAutos(vins);
    }
  };
  useEffect(() => {
    getAutomobiles();
  }, []);
  return (
    <Fragment>
      <div className="m-4">
        <div>
          <h1>Service History</h1>
        </div>
        <div className="input-group mb-3">
          <input
            type="search"
            className="form-control"
            placeholder="Search by vin..."
            aria-describedby="basic-addon1"
            name="Search"
            id="Search"
            value={search}
            onChange={changeSearch}
          />
          <label htmlFor="Search"></label>
          <div className="input-group-prepend">
            <button
              className="btn btn-outline-secondary"
              type="button"
              onClick={filteredServices}
            >
              Search
            </button>
          </div>
        </div>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>VIN</th>
              <th>Is VIP?</th>
              <th>Customer</th>
              <th>Date</th>
              <th>Time</th>
              <th>Technician</th>
              <th>Reason</th>
              <th>Status</th>
            </tr>
          </thead>
          <tbody>
            {filtered.map((appointment) => {
              const date = new Date(appointment.date_time).toLocaleDateString(
                "en-US",
                {
                  timeZone: "America/Los_Angeles",
                }
              );
              const time = new Date(appointment.date_time).toLocaleTimeString(
                "en-US",
                {
                  timeZone: "UTC",
                }
              );
              const vip = autos.includes(appointment.vin) ? "Yes" : "No";
              return (
                <tr key={appointment.id} className="align-center">
                  <td className="w-20 mh-100">{appointment.vin}</td>
                  <td className="w-20 mh-100">{vip}</td>
                  <td className="w-20 mh-100">{appointment.customer}</td>
                  <td className="w-20 mh-100">{date}</td>
                  <td className="w-20 mh-100">{time}</td>
                  <td className="w-20 mh-100">
                    {appointment.technician.first_name}{" "}
                    {appointment.technician.last_name}
                  </td>
                  <td className="w-20 mh-100">{appointment.reason}</td>
                  <td className="w-20 mh-100">{appointment.status}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </Fragment>
  );
}
