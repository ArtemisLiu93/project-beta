import { useEffect, useState, Fragment } from "react";
import { NavLink } from "react-router-dom";
export default function ServicesAppointments() {
  const [services, setServices] = useState([]);
  const getAppointments = async () => {
    const appointmentsResponse = await fetch(
      "http://localhost:8080/api/appointments/"
    );
    if (appointmentsResponse.ok) {
      const data = await appointmentsResponse.json();
      setServices(data.appointments);
    }
  };
  const [filtered, setFiltered] = useState([]);
  const filteredServices = () => {
    let filteredServices = services.filter(
      (service) => service.status === "Created"
    );
    setFiltered(filteredServices);
  };
  useEffect(() => {
    filteredServices();
  }, [services]);
  useEffect(() => {
    getAppointments();
  }, []);

  const [autos, setAutos] = useState([]);
  const getAutomobiles = async () => {
    const autosResponse = await fetch("http://localhost:8100/api/automobiles/");
    if (autosResponse.ok) {
      const autoData = await autosResponse.json();
      let vins = [];
      for (let i = 0; i < autoData.autos.length; i++) {
        vins.push(autoData.autos[i].vin);
      }
      setAutos(vins);
    }
  };
  useEffect(() => {
    getAutomobiles();
  }, []);

  const finishAppoitment = async (event) => {
    let id = event.target.value;
    const finishUrl = `http://localhost:8080/api/appointments/${id}/finish`;
    const fetchConfig = {
      method: "PUT",
      body: JSON.stringify(),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(finishUrl, fetchConfig);
    if (response.ok) {
      getAppointments();
    }
  };
  const cancelAppoitment = async (event) => {
    let id = event.target.value;
    const cancelUrl = `http://localhost:8080/api/appointments/${id}/cancel`;
    const fetchConfig = {
      method: "PUT",
      body: JSON.stringify(),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(cancelUrl, fetchConfig);
    if (response.ok) {
      getAppointments();
    }
  };
  return (
    <Fragment>
      <div className="m-4">
        <table className="table table-striped">
          <thead>
            <tr>
              <th>VIN</th>
              <th>Is VIP?</th>
              <th>Customer</th>
              <th>Date</th>
              <th>Time</th>
              <th>Technician</th>
              <th>Reason</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {filtered.map((appointment) => {
              const date = new Date(appointment.date_time).toLocaleDateString(
                "en-US",
                {
                  timeZone: "America/Los_Angeles",
                }
              );
              const time = new Date(appointment.date_time).toLocaleTimeString(
                "en-US",
                {
                  timeZone: "UTC",
                }
              );
              const vip = autos.includes(appointment.vin) ? "Yes" : "No";
              return (
                <tr key={appointment.id} className="align-center">
                  <td className="w-20 mh-100">{appointment.vin}</td>
                  <td className="w-20 mh-100">{vip}</td>
                  <td className="w-20 mh-100">{appointment.customer}</td>
                  <td className="w-20 mh-100">{date}</td>
                  <td className="w-20 mh-100">{time}</td>
                  <td className="w-20 mh-100">
                    {appointment.technician.first_name}{" "}
                    {appointment.technician.last_name}
                  </td>
                  <td className="w-20 mh-100">{appointment.reason}</td>
                  <td className="w-20 mh-100">
                    <button
                      className="btn btn-danger"
                      value={appointment.id}
                      onClick={cancelAppoitment}
                    >
                      Cancel
                    </button>
                  </td>
                  <td>
                    <button
                      className="btn btn-success"
                      value={appointment.id}
                      onClick={finishAppoitment}
                    >
                      Finish
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </Fragment>
  );
}
