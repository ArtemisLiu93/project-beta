import React, { useState } from "react";

export default function TechnicianForm() {
  const [first_name, setFirst] = useState("");
  const changeFirstName = (event) => {
    setFirst(event.target.value);
  };
  const [last_name, setLast] = useState("");
  const changeLastName = (event) => {
    setLast(event.target.value);
  };
  const [employee_id, setID] = useState("");
  const changeID = (event) => {
    setID(event.target.value);
  };
  const submitForm = async (event) => {
    event.preventDefault();
    const body = {
      first_name,
      last_name,
      employee_id,
    };
    const technicianUrl = "http://localhost:8080/api/technicians/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(body),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(technicianUrl, fetchConfig);
    if (response.ok) {
      const newTechnician = await response.json();
      console.log(newTechnician);
      setFirst("");
      setLast("");
      setID("");
    }
  };
  return (
    <div className="row">
      <div className="offset-3 col-4 m-auto">
        <div className="shadow-lg rounded p-4 mt-5">
          <h2 className="text-center mb-2 text-muted">
            Create a new Technician
          </h2>
          <form id="create-location-form" onSubmit={submitForm}>
            <div className="form-floating mb-3">
              <input
                placeholder="First name"
                required
                type="text"
                id="first_name"
                className="form-control"
                name="first_name"
                value={first_name}
                onChange={changeFirstName}
              />
              <label htmlFor="first_name">First Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                placeholder="Last Name"
                required
                type="text"
                id="last_name"
                className="form-control"
                name="last_name"
                value={last_name}
                onChange={changeLastName}
              />
              <label htmlFor="last_name">Last Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                placeholder="Employee ID"
                required
                type="text"
                id="employee_id"
                className="form-control"
                name="employee_id"
                value={employee_id}
                onChange={changeID}
              />
              <label htmlFor="employee_id">Employee ID</label>
            </div>
            <div className="text-center">
              <button className="btn btn-secondary">Create</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}
