import React, { useEffect, useState } from "react";
function AppointmentForm() {
  const [autoVIN, setVIN] = useState("");
  const vinChange = (event) => {
    console.log(event.target.value);
    setVIN(event.target.value);
  };
  const [customer, setCustomer] = useState("");
  const customerChange = (event) => {
    console.log(event.target.value);
    setCustomer(event.target.value);
  };
  const [date, setDate] = useState("");
  const dateChange = (event) => {
    console.log(event.target.value);
    setDate(event.target.value);
  };
  const [time, setTime] = useState("");
  const timeChange = (event) => {
    console.log(event.target.value);
    setTime(event.target.value);
  };
  const [tech, setTech] = useState("");
  const techChange = (event) => {
    console.log(event.target.value);
    setTech(event.target.value);
  };
  const [reason, setReason] = useState("");
  const reasonChange = (event) => {
    console.log(event.target.value);
    setReason(event.target.value);
  };
  const [technicians, setTechnicians] = useState([]);
  const getTechnicians = async () => {
    try {
      const response = await fetch("http://localhost:8080/api/technicians/");
      const data = await response.json();
      setTechnicians(data.technicians);
    } catch (err) {
      console.error(err);
    }
  };

  useEffect(() => {
    getTechnicians();
  }, []);
  const submitForm = async (event) => {
    event.preventDefault();
    const body = {
      date_time: date + " " + time,
      reason: reason,
      vin: autoVIN,
      customer: customer,
      technician: tech,
    };
    console.log(body);
    const appointmentUrl = "http://localhost:8080/api/appointments/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(body),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(appointmentUrl, fetchConfig);
    if (response.ok) {
      setVIN("");
      setDate("");
      setCustomer("");
      setTime("");
      setTech("");
      setReason("");
    }
  };
  return (
    <div className="row">
      <div className="offset-3 col-4 m-auto">
        <div className="shadow-lg rounded p-4 mt-5">
          <h2 className="text-center mb-2 text-muted">Create an Appointment</h2>
          <form id="create-location-form" onSubmit={submitForm}>
            <div className="form-floating mb-3">
              <input
                placeholder="Automobile VIN"
                required
                type="text"
                id="vin"
                className="form-control"
                name="vin"
                value={autoVIN}
                onChange={vinChange}
              />
              <label htmlFor="Automobile VIN">Automobile VIN</label>
            </div>
            <div className="form-floating mb-3">
              <input
                placeholder="Customer"
                required
                type="text"
                id="customer"
                className="form-control"
                name="customer"
                value={customer}
                onChange={customerChange}
              />
              <label htmlFor="Customer">Customer</label>
            </div>
            <div className="form-floating mb-3">
              <input
                placeholder="Date"
                required
                type="date"
                id="date_time"
                className="form-control"
                name="date_time"
                value={date}
                onChange={dateChange}
              />
              <label htmlFor="Date">Date</label>
            </div>
            <div className="form-floating mb-3">
              <input
                placeholder="Time"
                required
                type="time"
                id="date_time"
                className="form-control"
                name="date_time"
                value={time}
                onChange={timeChange}
              />
              <label htmlFor="Time">Time</label>
            </div>
            <div className="mb-3">
              <select
                required
                id="technician"
                className="form-select"
                name="technician"
                value={tech}
                onChange={techChange}
              >
                <option>Choose a technician</option>
                {technicians.map((technician) => {
                  return (
                    <option key={technician.id} value={technician.id}>
                      {`${technician.first_name} ${technician.last_name}`}
                    </option>
                  );
                })}
              </select>
            </div>
            <div className="form-floating mb-3">
              <input
                placeholder="Reason"
                required
                type="text"
                id="reason"
                className="form-control"
                name="reason"
                value={reason}
                onChange={reasonChange}
              />
              <label htmlFor="Reason">Reason</label>
            </div>
            <div className="text-center">
              <button className="btn btn-secondary">Create</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}
export default AppointmentForm;
