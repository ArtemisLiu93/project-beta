import { useEffect, useState, Fragment } from "react";
export default function TechniciansList() {
  const [technicians, setTechnicians] = useState([]);
  const getTechnicians = async () => {
    const techniciansResponse = await fetch(
      "http://localhost:8080/api/technicians/"
    );
    if (techniciansResponse.ok) {
      const technicians = await techniciansResponse.json();
      setTechnicians(technicians.technicians);
    }
  };
  useEffect(() => {
    getTechnicians();
  }, []);

  return (
    <Fragment>
      <div className="m-4">
        <div>
          <h1>Technicians</h1>
        </div>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Employee ID</th>
              <th>First Name</th>
              <th>Last Name</th>
            </tr>
          </thead>
          <tbody>
            {technicians.map((technician) => {
              return (
                <tr key={technician.id} className="align-center">
                  <td className="w-20 mh-100">{technician.employee_id}</td>
                  <td className="w-20 mh-100">{technician.first_name}</td>
                  <td className="w-20 mh-100">{technician.last_name}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </Fragment>
  );
}
