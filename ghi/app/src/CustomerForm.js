import React, { useEffect, useState } from 'react';

function CustomerForm() {
    const [first_name, setFirstName] = useState('');
    const [last_name, setLastName] = useState('');
    const [address, setAddress] = useState('');
    const [phone_number, setPhoneNumber] = useState('');

    const firstNameChange = (event) => {
        const value = event.target.value;
        setFirstName(value);
    }
    const lastNameChange = (event) => {
        const value = event.target.value;
        setLastName(value);
    }
    const addressChange = (event) => {
        const value = event.target.value;
        setAddress(value);
    }
    const phoneNumberChange = (event) => {
        const value = event.target.value;
        setPhoneNumber(value);
    }


    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.first_name = first_name;
        data.last_name = last_name;
        data.address = address;
        data.phone_number = phone_number;
        const customerUrl = 'http://localhost:8090/api/customers/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
        }
    };
    const response = await fetch(customerUrl, fetchConfig);
    if (response.ok) {
        const newCustomer = await response.json();
        setFirstName('');
        setLastName('');
        setAddress('');
        setPhoneNumber('');
        }
    };


    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add A New Customer</h1>
                    <form id="create-shoes-form" onSubmit={handleSubmit}>
                        <div className="form-floating mb-3">
                            <input
                                value={first_name}
                                placeholder="First Name"
                                required
                                type="text"
                                name="first_name"
                                id="first_name"
                                className="form-control"
                                onChange={firstNameChange}
                            />
                            <label htmlFor="first_name">First Name</label>
                        </div>

                        <div className="form-floating mb-3">
                            <input
                                value={last_name}
                                placeholder="Last Name"
                                required
                                type="text"
                                name="last_name"
                                id="last_name"
                                className="form-control"
                                onChange={lastNameChange}
                            />
                            <label htmlFor="last_name">Last Name</label>
                        </div>

                        <div className="form-floating mb-3">
                            <input
                                value={address}
                                placeholder="Address"
                                required
                                type="text"
                                id="address"
                                name="address"
                                className="form-control"
                                onChange={addressChange}
                            />
                            <label htmlFor="address">Address</label>
                        </div>

                        <div className="mb-3">
                            <label htmlFor="phone_number">Phone Number</label>
                            <input
                                value={phone_number}
                                required
                                id="phone_number"
                                name="phone_number"
                                className="form-control"
                                type="text"
                                onChange={phoneNumberChange}
                            />
                        </div>

                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default CustomerForm;