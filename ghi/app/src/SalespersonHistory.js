import React, { useState, useEffect } from "react";

function SalespersonHistory() {
    const [sales, setSales] = useState([]);
    const [people, setPeople] = useState([]);
    const [filteredSales, setFilteredSales] = useState([]);
    const [person, setPerson] = useState('');
    


    const fetchSales = async () => {
        const response = await fetch("http://localhost:8090/api/sales/");
        if(response.ok) {
            const data = await response.json();
            setSales(data.sales);
        }
    }

    const filter = (event) => {
        const salesperson = event.target.value;
        setPerson(salesperson)

        const specificPerson = []
        for (let sale of sales) {
            if((sale.salesperson.first_name + " " + sale.salesperson.last_name) == salesperson)
                specificPerson.push(sale)
        }
        setFilteredSales(specificPerson);
    }


    const fetchSalesPeople = async () => {
        const response = await fetch("http://localhost:8090/api/salespeople/");
        if(response.ok) {
            const data = await response.json();
            setPeople(data.salespeople);
        }
    }

    useEffect(() => {
        fetchSales();
        fetchSalesPeople();
    }, []);


    return (
    <div className="m-4">
        <h1>Salespeople History</h1>
        <div className="form-floating mb-3">
            <select 
                placeholder="Salesperson"
                required
                name="people"
                id="people"
                className="form-select"
                onChange={filter}
                value={person}
                >
                {people.map(person => {
                    return (
                        <option key={person.id}>
                            {person.first_name} {person.last_name}
                        </option>
                    );
                })}
            </select>
        </div>
        <table className="table table-striped">
        <thead>
            <tr>
            <th>Salesperson</th>
            <th>Customer</th>
            <th>VIN</th>
            <th>Price</th>
            </tr>
        </thead>
        <tbody>
            {filteredSales?.map(sale => (
                    <tr key={sale.id}>
                        <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                        <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                        <td>{sale.automobile.vin} </td>
                        <td>${sale.price} </td>
                        <td>{ }</td>
                    </tr>
                ))}
            </tbody>
        </table>
    </div>
    );
}

export default SalespersonHistory;